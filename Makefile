PUBLISH_PATH="/lysator/lyswww/projects/roxen/inbyggda-hack/pi"

index.html : index.md make.sh
	./make.sh

.PHONY: publish
publish: index.html
	cp lyslogo-liten.png index.html style.css ${PUBLISH_PATH}
	cp -R patches ${PUBLISH_PATH}
