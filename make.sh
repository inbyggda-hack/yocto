#!/usr/bin/env bash

function varexp () {
	cat $@ | sed \
		-e 's/RESPONSIBLE/Andreas Kempe \&lt;kempe@lysator.liu.se\&gt;/g'
}

cat > index.html << EOF
<!doctype html>
<html>
	<head>
		<title>Yocto</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
	</head>
	<body>
		<div class="content">
			<img class="header-logo" src="lyslogo-liten.png" alt="Lysatorlogo"/>
			$(varexp index.md | pandoc -f markdown -t html)
			<div class="page-footer">Last updated $(date)</div>
		</div>
	</body>
</html>
EOF
